package at.ac.tuwien.trackme.runners

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import at.ac.tuwien.trackme.TrackMeTestApplication

class TestApplicationRunner: AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TrackMeTestApplication::class.java.name, context)
    }
}