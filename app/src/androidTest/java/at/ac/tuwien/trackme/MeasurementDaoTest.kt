package at.ac.tuwien.trackme

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import at.ac.tuwien.trackme.data.source.MeasurementDao
import at.ac.tuwien.trackme.data.source.TrackMeDatabase
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations
import androidx.room.Room
import at.ac.tuwien.trackme.LiveDataTestUtil.getValue
import at.ac.tuwien.trackme.TestData.Companion.MEASUREMENT_ENTITY
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.Assert.assertTrue

class MeasurementDaoTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: TrackMeDatabase
    private lateinit var measurementDao: MeasurementDao

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val context = InstrumentationRegistry.getInstrumentation().context
        database = Room.inMemoryDatabaseBuilder(context, TrackMeDatabase::class.java).allowMainThreadQueries().build()
        measurementDao = database.measurementDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun getMeasurementsAfterInserted() {
        val testMeasurement = MEASUREMENT_ENTITY

        runBlocking {
            measurementDao.insertMeasurement(testMeasurement)
        }

        val daoMeasurements = getValue(measurementDao.getMeasurements())

        assert(daoMeasurements.size == 1)
    }

    @Test
    fun getMeasurementsWhenNoMeasurementInserted() {
        val measurements = getValue(measurementDao.getMeasurements())
        assertTrue(measurements.isEmpty())
    }

}

