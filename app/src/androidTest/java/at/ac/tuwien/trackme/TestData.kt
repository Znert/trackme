package at.ac.tuwien.trackme

import at.ac.tuwien.trackme.data.Measurement
import java.util.*

class TestData {

    companion object {

        var MEASUREMENT_ENTITY = Measurement(
            1,
            Calendar.getInstance().time.toString(),
            10.0,
            10.0,
            20,
            "param1",
            11.0,
            11.0,
            21,
            3
        )
        var MEASUREMENT_ENTITY2 = Measurement(
            2,
            Calendar.getInstance().time.toString(),
            20.0,
            20.0,
            20,
            "param1",
            20.0,
            20.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY3 = Measurement(
            3,
            Calendar.getInstance().time.toString(),
            30.0,
            30.0,
            20,
            "param1",
            30.0,
            30.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY4 = Measurement(
            4,
            Calendar.getInstance().time.toString(),
            40.0,
            40.0,
            20,
            "param1",
            40.0,
            40.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY5 = Measurement(
            5,
            Calendar.getInstance().time.toString(),
            50.0,
            50.0,
            20,
            "param1",
            50.0,
            50.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY6 = Measurement(
            6,
            Calendar.getInstance().time.toString(),
            60.0,
            60.0,
            20,
            "param1",
            60.0,
            60.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY7 = Measurement(
            7,
            Calendar.getInstance().time.toString(),
            70.0,
            70.0,
            20,
            "param1",
            70.0,
            70.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY8 = Measurement(
            8,
            Calendar.getInstance().time.toString(),
            80.0,
            80.0,
            20,
            "param1",
            80.0,
            80.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY9 = Measurement(
            9,
            Calendar.getInstance().time.toString(),
            90.0,
            90.0,
            20,
            "param1",
            90.0,
            90.0,
            20,
            0
        )

        var MEASUREMENT_ENTITY10 = Measurement(
            10,
            Calendar.getInstance().time.toString(),
            100.0,
            100.0,
            20,
            "param1",
            100.0,
            100.0,
            20,
            0
        )

        var MEASUREMENTS_SHORT = listOf(MEASUREMENT_ENTITY, MEASUREMENT_ENTITY2)
        var MEASUREMENTS_LONG = listOf(MEASUREMENT_ENTITY, MEASUREMENT_ENTITY2, MEASUREMENT_ENTITY3, MEASUREMENT_ENTITY4, MEASUREMENT_ENTITY5, MEASUREMENT_ENTITY6, MEASUREMENT_ENTITY7, MEASUREMENT_ENTITY8, MEASUREMENT_ENTITY9, MEASUREMENT_ENTITY10)

    }
}