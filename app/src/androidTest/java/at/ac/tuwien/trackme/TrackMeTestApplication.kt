package at.ac.tuwien.trackme

import android.location.LocationManager
import at.ac.tuwien.trackme.services.MLSApi

class TrackMeTestApplication: TrackMeApplication() {

    var mlsAPIMock: MLSApi? = null
    var locationManagerMock: LocationManager? = null

    override fun getMLSAPi(): MLSApi {
        return mlsAPIMock
            ?: super.getMLSAPi()
    }

    override fun getLocationManager(): LocationManager {
        return locationManagerMock
            ?: super.getLocationManager()
    }
}