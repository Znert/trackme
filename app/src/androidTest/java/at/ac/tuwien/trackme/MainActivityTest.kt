package at.ac.tuwien.trackme

import android.Manifest
import android.os.SystemClock
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import at.ac.tuwien.trackme.TestData.Companion.MEASUREMENTS_LONG
import at.ac.tuwien.trackme.TestData.Companion.MEASUREMENTS_SHORT
import at.ac.tuwien.trackme.data.Measurement
import at.ac.tuwien.trackme.data.source.MeasurementDao
import at.ac.tuwien.trackme.data.source.TrackMeDatabase
import at.ac.tuwien.trackme.utils.Location
import at.ac.tuwien.trackme.utils.MLSApiResponse
import at.ac.tuwien.trackme.utils.MockingUtil
import at.ac.tuwien.trackme.utils.SwipeRevealLayout
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.containsString
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.math.floor


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        /**
         * We must mock the DAO layer here to inject the mock before the activity is started
         */
        override fun beforeActivityLaunched() {
            super.beforeActivityLaunched()
            // Mock the database layer
            dao = MockingUtil.mockDAO(liveData)
            trackMeDatabase = MockingUtil.mockTrackMeDatabase(dao)

            // Set the singleton used to the mocked database
            TrackMeDatabase.INSTANCE = trackMeDatabase
        }
    }

    @get:Rule
    val permissionRule = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    private lateinit var trackMeDatabase: TrackMeDatabase

    private lateinit var dao: MeasurementDao

    private var liveData = MutableLiveData<List<Measurement>>()

    @Test
    fun createMeasurementAndDisplayList() {
        liveData.postValue(emptyList())

        val mlsApi = MockingUtil.mockMLSApi(
            MLSApiResponse(
                Location(100.0, 100.0),
                10.0f
            )
        )
        (activityRule.activity.application as TrackMeTestApplication).mlsAPIMock = mlsApi

        val locationManager = MockingUtil.mockLocationService(
            100.0, 100.0, 10.0f
        )
        (activityRule.activity.application as TrackMeTestApplication).locationManagerMock =
            locationManager

        this.navigateToListView()

        val recyclerView = activityRule.activity.findViewById<RecyclerView>(R.id.history_list)
        Assert.assertEquals(recyclerView.adapter!!.itemCount, 0)

        onView(withId(R.id.add_measurement)).perform(click())
        Thread.sleep(1000)

        Assert.assertEquals(recyclerView.adapter!!.itemCount, 1)

        verify(locationManager, times(1)).requestSingleUpdate(any<String>(), any(), any())
        verify(mlsApi, times(1)).getLocation(any(), any())
    }

    @Test
    fun displayListAndClickOnFirstItem() {
        liveData.postValue(MEASUREMENTS_SHORT)
        this.navigateToListView()

        // First click on the first item
        onView(withId(R.id.history_list)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )

        // Verify that the text matches the first measurement
        val m = MEASUREMENTS_SHORT[0]
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Bericht #${m.id}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Datum: ${m.timestamp}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Long: ${floor(m.gps_longitude)}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Lat: ${floor(m.gps_latitude)}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Long: ${floor(m.geo_longitude)}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Lat: ${floor(m.geo_latitude)}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Lat: ${floor(m.geo_latitude)}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Genauigkeit: ${m.precision}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Genauigkeit: ${m.geo_precision}")))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString(m.params)))))
        )
        onView(withId(R.id.history_list)).check(
            matches(atPosition(0, hasDescendant(withText(containsString("Unterschied MLS GPS: ${m.diff_mls_gps}")))))
        )
    }

    @Test
    fun displayListAndScroll() {
        liveData.postValue(MEASUREMENTS_LONG)

        navigateToListView()

        val recyclerView = activityRule.activity.findViewById<RecyclerView>(R.id.history_list)
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val adapter = recyclerView.adapter!!

        // First verify that the list is scrollable
        Assert.assertTrue(layoutManager.findLastCompletelyVisibleItemPosition() < adapter.itemCount - 1)

        // Scroll to the last element
        onView(withId(R.id.history_list))
            .perform(
                RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(adapter.itemCount - 1)
            )

        // Check if the last element is now visible
        Assert.assertEquals(
            adapter.itemCount - 1,
            layoutManager.findLastCompletelyVisibleItemPosition()
        )
    }

    @Test
    fun deleteMeasurement() {
        liveData.postValue(MEASUREMENTS_SHORT)
        this.navigateToListView()

        onView(withId(R.id.history_list)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                object : ViewAction {
                    override fun getDescription(): String {
                        return "Open the swipe-to-reveal item"
                    }

                    override fun getConstraints(): Matcher<View> {
                        return allOf(isDisplayed(), isAssignableFrom(View::class.java))
                    }

                    override fun perform(uiController: UiController?, view: View?) {
                        view?.let {
                            val child = view.findViewById<SwipeRevealLayout>(R.id.swipe_reveal)
                            child.open(false)
                        }
                    }
                }
            )
        )

        onView(withId(R.id.history_list)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                actionOnChild(click(), R.id.delete_button)
            )
        ).check { view, noViewFoundException ->
            if (noViewFoundException != null) {
                throw noViewFoundException
            }

            val recycler = view as RecyclerView
            val adapter = recycler.adapter
            assertThat(adapter?.itemCount, `is`(1))
        }

        runBlocking { verify(dao).deleteMeasurement(MEASUREMENTS_SHORT[1]) }
    }

    private fun navigateToListView() {
        val matcher = allOf(withText("Berichte"), isDescendantOfA(withId(R.id.tabs)))
        onView(matcher).perform(click())
        SystemClock.sleep(800)
        onView(withId(R.id.history_list)).check(matches(isCompletelyDisplayed()))
    }

    /**
     * Taken from
     *
     * https://medium.com/@xabaras/dealing-with-actions-and-checks-on-children-of-a-recyclerviews-item-in-espresso-tests-dabd93361810
     */
    private fun actionOnChild(action: ViewAction, childId: Int): ViewAction {
        return object : ViewAction {
            override fun getDescription(): String {
                return "Perform action on the view whose id is passed in"
            }

            override fun getConstraints(): Matcher<View> {
                return allOf(isDisplayed(), isAssignableFrom(View::class.java))
            }

            override fun perform(uiController: UiController?, view: View?) {
                view?.let {
                    val child: View = it.findViewById(childId)
                    action.perform(uiController, child)
                }
            }
        }
    }

    /**
     * Taken from
     *
     * https://stackoverflow.com/a/34795431
     */
    private fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?>? {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

}