package at.ac.tuwien.trackme.utils

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.lifecycle.MutableLiveData
import at.ac.tuwien.trackme.data.Measurement
import at.ac.tuwien.trackme.data.source.MeasurementDao
import at.ac.tuwien.trackme.data.source.TrackMeDatabase
import at.ac.tuwien.trackme.services.MLSApi
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MockingUtil {

    /**
     * Returns a mocked DAO which implements all methods of the DAO correctly.
     */
    fun mockDAO(liveData: MutableLiveData<List<Measurement>>): MeasurementDao {
        return mock {

            // Mock call to get Measurements
            on { getMeasurements() } doReturn liveData

            // Mock call to insertMeasurement
            on { runBlocking { insertMeasurement(any()) } } doAnswer {
                val measurement = it.arguments[0] as Measurement

                val newValue = mutableListOf<Measurement>()
                newValue.addAll(liveData.value.orEmpty())
                newValue.add(measurement)

                liveData.postValue(newValue.toList())
            }

            // Mock call to deleteAll
            on { runBlocking { deleteAll() } } doAnswer {
                liveData.postValue(emptyList())
            }

            // Mock call to deleteMeasurement
            on { runBlocking { deleteMeasurement(any()) } } doAnswer {
                val measurement = it.arguments[0] as Measurement

                val newValue = mutableListOf<Measurement>()
                newValue.addAll(liveData.value.orEmpty())
                newValue.remove(measurement)

                liveData.postValue(newValue.toList())
            }

            // Mock call to getMeasurementById
            on { runBlocking { getMeasurementById(any()) } } doAnswer {
                val id = it.arguments[0] as Int

                return@doAnswer liveData.value?.find { m -> m.id == id }
            }

        }
    }

    /**
     * Returns a fully mocked TrackMeDatabase object.
     */
    fun mockTrackMeDatabase(dao: MeasurementDao): TrackMeDatabase {
        return mock {
            on { measurementDao() } doReturn dao
        }
    }

    fun mockMLSApi(response: MLSApiResponse): MLSApi {

        val callMock = mock<Call<MLSApiResponse>> {
            on { isCanceled } doReturn false
            on { isExecuted } doReturn true
            on { enqueue(any()) } doAnswer {
                val callback = it.arguments[0] as Callback<MLSApiResponse>

                callback.onResponse(
                    mock(),
                    Response.success(response)
                )
            }
        }

        return mock {
            on { getLocation(any(), any()) } doReturn callMock
        }
    }

    /**
     * Returns a mocked LocationManager
     */
    fun mockLocationService(long: Double, lat: Double, accuracy: Float): LocationManager {
        val location = Location(LocationManager.GPS_PROVIDER)
        location.latitude = lat
        location.longitude = long
        location.accuracy = accuracy
        return mock {
            on { isProviderEnabled(any()) } doReturn true
            on { getLastKnownLocation(any()) } doReturn location
            on { requestSingleUpdate(any<String>(), any(), any())} doAnswer {
                val locationListener = it.arguments[1] as LocationListener
                locationListener.onLocationChanged(location)
            }
            on { requestLocationUpdates(any(), any(), any(), any<LocationListener>()) } doAnswer {
                val locationListener = it.arguments[1] as LocationListener
                locationListener.onLocationChanged(location)
            }
        }
    }

}