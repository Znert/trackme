package at.ac.tuwien.trackme.services

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

/**
 * This service handles asynchronously requesting permissions using callbacks upon successfully
 * getting the permission.
 */
class PermissionService(private val context: Context, private val requester: PermissionRequester) {

    // A map of pending tasks. They are grouped by the permission they are requiring
    private val pendingTasks = mutableMapOf<String, MutableList<() -> Unit>>()

    /**
     * This function will execute a given task only after the permission has been granted.
     *
     * If a permission has been granted already, the task is executed immediately. If the permission
     * is missing, the task will be added to a queue which is executed as soon as the permission has
     * been granted.
     */
    @Synchronized
    fun waitForPermission(permission: String, task: () -> Unit) {
        if (ContextCompat.checkSelfPermission(this.context, permission) != PackageManager.PERMISSION_GRANTED) {
            var list = pendingTasks[permission]
            if (list == null) {
                val newList = mutableListOf<() -> Unit>()
                pendingTasks[permission] = newList
                list = newList
            }
            list.add(task)

            this.requester.requestPermission(permission)
        } else {
            task()
        }
    }

    /**
     * This method must be called by an activity when a permission has been granted or denied.
     *
     * If the permission has been granted (granted is true), the task queue for that permission is
     * executed one by one. After that the task queue is removed from the map to avoid double
     * execution. If the permission has been denied (granted is false), the task queue for that
     * permission is removed and no tasks are executed.
     */
    @Synchronized
    fun permissionResponse(permission: String, granted: Boolean) {

        if (!granted) {
            pendingTasks.remove(permission)
            return
        }

        val tasks = pendingTasks[permission] ?: return

        tasks.forEach { it() }

        pendingTasks.remove(permission)
    }

    /**
     * This interface provides method for requesting a permission. Note that
     * {@link permissionResponse} must be called on the caller of requestPermission.
     */
    interface PermissionRequester {
        fun requestPermission(permission: String)
    }

    /**
     * Interface for providing a PermissionService instance.
     */
    interface PermissionServiceManager {
        fun getPermissionService(): PermissionService
    }
}