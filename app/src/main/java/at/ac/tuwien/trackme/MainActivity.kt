package at.ac.tuwien.trackme

import android.app.Application
import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import at.ac.tuwien.trackme.data.source.TrackMeDatabase
import at.ac.tuwien.trackme.services.LocationService
import at.ac.tuwien.trackme.services.PermissionService
import at.ac.tuwien.trackme.utils.EncryptionUtil
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*


/**
 * The main activity is the main - and only - entry point for this app.
 *
 * It must implement the PermissionServiceManager and PermissionRequester interface in order to
 * provide methods for delaying functions until certain permissions are granted.
 */
class MainActivity : AppCompatActivity(), PermissionService.PermissionServiceManager,
    PermissionService.PermissionRequester {

    // The location manager used for getting GPS data
    private lateinit var locationManager: LocationManager
    // The view model to store the data in the database
    private lateinit var historyViewModel: HistoryViewModel
    // The location service creating new measurements
    private lateinit var locationService: LocationService

    private var bound: Boolean = false
    private var connection = object  : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as LocationService.LocationServiceBinder
            locationService = binder.getService()
            locationService.historyViewModel = historyViewModel
            locationService.context = this@MainActivity
            bound = true

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            bound = false
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as TrackMeApplication).setPermissionService(this, this)

        initServices()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(MeasurementFragment(), "Messung")
        adapter.addFragment(HistoryFragment(), "Berichte")
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)

        // Set the click handler of the plus button
        val button = findViewById<FloatingActionButton>(R.id.add_measurement)
        button.setOnClickListener {
            val spinner = ProgressDialog.show(
                this,
                "Neue Messung wird durchgeführt",
                "Dies kann einige Zeit dauern...",
                true
            )
            locationService.newMeasurement {
                spinner.dismiss()

                if (it != null) {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Fehler beim Messen der Daten")
                        .setMessage(it)
                        .setCancelable(false)
                        .setPositiveButton("OK") { dialog, _ ->
                            dialog.dismiss()
                        }
                    val alert: AlertDialog = builder.create()
                    alert.show()
                } else {
                    Toast.makeText(
                        this,
                        "Messung erfolgreich durchgeführt",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu!!.add("Datenbank verschlüsseln")
        return !EncryptionUtil.isEncrypted(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Don't need to check the menu item for now, we only have one option
        TrackMeDatabase.encryptDatabase(this)
        (application as TrackMeApplication).getApiKeyService().encryptApiKey()

        recreate()

        return true
    }

    override fun onStart() {
        super.onStart()
        Intent(this, LocationService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        bound = false
    }

    /**
     * Initializes all services used in this application
     */
    private fun initServices() {
        locationManager = (application as TrackMeApplication).getLocationManager()

        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel::class.java)
    }

    /**
     * This method is called by the PermissionService when it wants to request a new permission.
     *
     * The permission is requested using the ActivityCompat with this activity. This means that
     * {@link onRequestPermissionsResult} is called upon response.
     */
    override fun requestPermission(permission: String) {
        ActivityCompat.requestPermissions(this, arrayOf(permission), 1)
    }

    /**
     * This method is called whenever a permission request is responded to.
     *
     * As permissions are managed by the PermissionService it need to notify the service about
     * the granted permissions.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for (i in permissions.indices) {
            this.getPermissionService().permissionResponse(
                permissions[i],
                grantResults[i] == PackageManager.PERMISSION_GRANTED
            )
        }
    }

    /**
     * Getter for the PermissionService
     */
    override fun getPermissionService() = (application as TrackMeApplication).getPermissionService()
}
