package at.ac.tuwien.trackme.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDateTime
import java.util.*

@Entity(tableName = "Measurements")
data class Measurement constructor(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "timestamp") val timestamp: String,
    @ColumnInfo(name = "gps_longitude") val gps_longitude: Double,
    @ColumnInfo(name = "gps_latitude") val gps_latitude: Double,
    @ColumnInfo(name = "precision") val precision: Int,
    @ColumnInfo(name = "params") val params: String,
    @ColumnInfo(name = "geo_longitude") val geo_longitude: Double,
    @ColumnInfo(name = "geo_latitude") val geo_latitude: Double,
    @ColumnInfo(name = "geo_precision") val geo_precision: Int,
    @ColumnInfo(name = "diff_mls_gps") val diff_mls_gps: Int

) {
    override fun toString(): String {
        return "Bericht #$id\nAufnahmedatum: $timestamp\nGPS Koordinaten:\n\tLongitude: " +
                "$gps_longitude\n\tLatitude: $gps_latitude\n" +
                "Genauigkeit: ${precision}m\n" +
                "MLI API Request:" +
                "\n\tParameter: $params\n\tErgebnis:\nLongitude: $geo_longitude\nLatitude: $geo_latitude\n" +
                "Genauigkeit: ${precision}m\n" +
                "Unterschied zwischen MLS und GPS Position: ${diff_mls_gps}m"
    }
}