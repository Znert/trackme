package at.ac.tuwien.trackme.utils

import android.content.Context
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import at.ac.tuwien.trackme.TrackMeApplication
import com.commonsware.cwac.saferoom.SQLCipherUtils
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.IvParameterSpec


object EncryptionUtil {

    private const val databaseName = TrackMeApplication.CONSTANTS.DATABASE_NAME // name of the database
    private const val alias = "TrackMeKey" // alias-name of the key in the keystore
    private const val filename = "/pw"    // filename of the password
    private const val keyStoreName = "AndroidKeyStore"    // name of the keystore

    fun isEncrypted(context: Context): Boolean {
        val state = SQLCipherUtils.getDatabaseState(context.applicationContext, databaseName)
        return state.name == SQLCipherUtils.State.ENCRYPTED.name
    }

    /**
     * Method to create a new encrypted Database (+ Password + encryption Keys)
     * Reference: https://github.com/commonsguy/cwac-saferoom/blob/master/demo/src/main/java/com/commonsware/android/auth/note/RxPassphrase.kt
     */
    fun createEncryptedDB(context: Context) {
        // if db is already encrypted, we do not want to generate a new key
        if (isEncrypted(context))
            return

        // create new random password
        val random = SecureRandom()
        val b = ByteArray(128)
        random.nextBytes(b)

        val encrypted = this.encrypt(b, this.alias)

        val file = File(context.filesDir, filename)
        file.createNewFile()
        if (file.exists()) {
            val o = FileOutputStream(file)
            o.write(encrypted)
            o.close()
        } else {
            throw IllegalAccessException("No file with name $filename")
        }

        SQLCipherUtils.encrypt(context, databaseName, b)
    }

    fun encrypt(b: ByteArray, alias: String): ByteArray {

        // create generator for the key and set some parameter specifications for it
        // Here we use AES/CBC/PKCS#7
        val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, keyStoreName)
        val keyGenParameterSpec = KeyGenParameterSpec.Builder(
            alias,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
            .build()
        keyGenerator.init(keyGenParameterSpec)

        // generate secret key to initialize a cipher used for encryption
        val secretKey = keyGenerator.generateKey()
        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")

        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        var encrypted = cipher.doFinal(b)

        return byteArrayOf(cipher.iv.size.toByte()) + cipher.iv + encrypted
    }

    fun decrypt(b: ByteArray, alias: String): ByteArray {
        // get instance of keystore
        val keyStore = KeyStore.getInstance(keyStoreName)
        keyStore.load(null)
        // get key from keystore with name alias
        val secretKeyEntry: KeyStore.SecretKeyEntry =
            keyStore.getEntry(alias, null) as KeyStore.SecretKeyEntry?
                ?: return ByteArray(0)
        val secretKey = secretKeyEntry.secretKey

        // recover the iv
        val ivSize = b[0] // the first byte is the length of the iv
        val iv = b.slice(1..ivSize).toByteArray() // read the iv
        // the rest of the file is payload
        val decrypt = b.slice(ivSize + 1 until b.size).toByteArray()

        // get cipher instance to decrypt key
        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        val spec = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, secretKey, spec)

        return cipher.doFinal(decrypt)
    }

    /**
     * Returns the passphrase of the database as byte array.
     *
     * Reference: https://medium.com/@josiassena/using-the-android-keystore-system-to-store-sensitive-information-3a56175a454b
     */
    fun getDBPassword(context: Context): ByteArray {

        // if the db is not encrypted, return empty
        if (!isEncrypted(context))
            return ByteArray(0)

        // open key file and read it, then decrypt the key using the cipher
        val file = File(context.filesDir, filename)
        if (file.exists()) {
            // Read the full file  containing the password first
            val decryptPlusHeader = ByteArray(file.length().toInt())
            val bufferedInputStream = BufferedInputStream(FileInputStream(file))
            bufferedInputStream.read(decryptPlusHeader, 0, decryptPlusHeader.size)
            bufferedInputStream.close()
            return this.decrypt(decryptPlusHeader, this.alias)
        }

        return ByteArray(0)

    }


}