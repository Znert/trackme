package at.ac.tuwien.trackme.utils

import android.os.Build
import android.telephony.CellLocation
import android.telephony.TelephonyManager
import android.telephony.cdma.CdmaCellLocation
import android.telephony.gsm.GsmCellLocation
import com.google.gson.JsonIOException
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException

/**
 * This code was taken and modified from the MozStumpler App:
 *
 * https://github.com/mozilla/MozStumbler/blob/dev/libraries/stumbler/src/main/java/org/mozilla/mozstumbler/service/stumblerthread/scanners/cellscanner/CellInfo.java
 */
class CellInfo {
    var cellRadio: String? = null
        private set
    var mcc = 0
        private set
    var mnc = 0
        private set
    var cid = 0
        private set
    var lac = 0
        private set

    fun setSignalStrength(signalStrength: Int) {
        mSignalStrength = signalStrength
    }

    private var mSignalStrength = 0
    private var mAsu = 0
    private var mTa = 0
    var psc = 0
        private set

    init {
        reset()
    }

    val isCellRadioValid: Boolean
        get() = cellRadio != null && cellRadio!!.isNotEmpty() && cellRadio != "0"

    fun toJsonObject(): JsonObject {
        val obj = JsonObject()
        try {
            obj.addProperty("radioType", cellRadio)
            obj.addProperty("cellId", cid)
            obj.addProperty("locationAreaCode", lac)
            obj.addProperty("mobileCountryCode", mcc)
            obj.addProperty("mobileNetworkCode", mnc)
            if (mSignalStrength != UNKNOWN_SIGNAL_STRENGTH) obj.addProperty(
                "signalStrength",
                mSignalStrength
            )
            if (mTa != UNKNOWN_CID) obj.addProperty("timingAdvance", mTa)
            if (psc != UNKNOWN_CID) obj.addProperty("psc", psc)
            if (mAsu != UNKNOWN_ASU) obj.addProperty("asu", mAsu)
        } catch (jsonE: JsonIOException) {
            throw IllegalStateException(jsonE)
        } catch (jsonE: JsonSyntaxException) {
            throw IllegalStateException(jsonE)
        }
        return obj
    }

    val cellIdentity: String
        get() = (cellRadio
                + " " + mcc
                + " " + mnc
                + " " + lac
                + " " + cid
                + " " + psc)

    fun reset() {
        cellRadio = CELL_RADIO_GSM
        mcc = UNKNOWN_CID
        mnc = UNKNOWN_CID
        lac = UNKNOWN_LAC
        cid = UNKNOWN_CID
        mSignalStrength = UNKNOWN_SIGNAL_STRENGTH
        mAsu = UNKNOWN_ASU
        mTa = UNKNOWN_CID
        psc = UNKNOWN_CID
    }

    fun setCellLocation(
        cl: CellLocation,
        networkType: Int,
        networkOperator: String?,
        signalStrength: Int
    ) {
        if (cl is GsmCellLocation) {
            val lac: Int
            val cid: Int
            val gcl = cl
            reset()
            cellRadio = getCellRadioTypeName(networkType)
            setNetworkOperator(networkOperator)
            lac = gcl.lac
            cid = gcl.cid
            if (lac >= 0) this.lac = lac
            if (cid >= 0) this.cid = cid
            if (Build.VERSION.SDK_INT >= 9) {
                val psc = gcl.psc
                if (psc >= 0) this.psc = psc
            }
            if (signalStrength != null) {
                mSignalStrength = signalStrength
            }
        } else if (cl is CdmaCellLocation) {
            val cdl = cl
            reset()
            cellRadio = getCellRadioTypeName(networkType)
            setNetworkOperator(networkOperator)
            mnc = cdl.systemId
            lac = cdl.networkId
            cid = cdl.baseStationId
            if (signalStrength != null) {
                mSignalStrength = signalStrength
            }
        } else {
            throw IllegalArgumentException("Unexpected CellLocation type: " + cl.javaClass.name)
        }
    }

    fun setGsmCellInfo(mcc: Int, mnc: Int, lac: Int, cid: Int, asu: Int) {
        cellRadio = CELL_RADIO_GSM
        this.mcc = if (mcc != Int.MAX_VALUE) mcc else UNKNOWN_CID
        this.mnc = if (mnc != Int.MAX_VALUE) mnc else UNKNOWN_CID
        this.lac = if (lac != Int.MAX_VALUE) lac else UNKNOWN_LAC
        this.cid = if (cid != Int.MAX_VALUE) cid else UNKNOWN_CID
        mAsu = asu
    }

    fun setWcdmaCellInfo(mcc: Int, mnc: Int, lac: Int, cid: Int, psc: Int, asu: Int) {
        cellRadio = CELL_RADIO_WCDMA
        this.mcc = if (mcc != Int.MAX_VALUE) mcc else UNKNOWN_CID
        this.mnc = if (mnc != Int.MAX_VALUE) mnc else UNKNOWN_CID
        this.lac = if (lac != Int.MAX_VALUE) lac else UNKNOWN_LAC
        this.cid = if (cid != Int.MAX_VALUE) cid else UNKNOWN_CID
        this.psc = if (psc != Int.MAX_VALUE) psc else UNKNOWN_CID
        mAsu = asu
    }

    /**
     * @param mcc Mobile Country Code, Integer.MAX_VALUE if unknown
     * @param mnc Mobile Network Code, Integer.MAX_VALUE if unknown
     * @param ci  Cell Identity, Integer.MAX_VALUE if unknown
     * @param psc Physical Cell Id, Integer.MAX_VALUE if unknown
     * @param lac Tracking Area Code, Integer.MAX_VALUE if unknown
     * @param asu Arbitrary strength unit
     * @param ta  Timing advance
     */
    fun setLteCellInfo(
        mcc: Int,
        mnc: Int,
        ci: Int,
        psc: Int,
        lac: Int,
        asu: Int,
        ta: Int
    ) {
        cellRadio = CELL_RADIO_LTE
        this.mcc = if (mcc != Int.MAX_VALUE) mcc else UNKNOWN_CID
        this.mnc = if (mnc != Int.MAX_VALUE) mnc else UNKNOWN_CID
        this.lac = if (lac != Int.MAX_VALUE) lac else UNKNOWN_LAC
        cid = if (ci != Int.MAX_VALUE) ci else UNKNOWN_CID
        this.psc = if (psc != Int.MAX_VALUE) psc else UNKNOWN_CID
        mAsu = asu
        mTa = ta
    }

    fun setCdmaCellInfo(baseStationId: Int, networkId: Int, systemId: Int, dbm: Int) {
        cellRadio = CELL_RADIO_CDMA
        mnc = if (systemId != Int.MAX_VALUE) systemId else UNKNOWN_CID
        lac =
            if (networkId != Int.MAX_VALUE) networkId else UNKNOWN_LAC
        cid =
            if (baseStationId != Int.MAX_VALUE) baseStationId else UNKNOWN_CID
        mSignalStrength = dbm
    }

    fun setNetworkOperator(mccMnc: String?) {
        require(!(mccMnc == null || mccMnc.length < 5 || mccMnc.length > 8)) { "Bad mccMnc: $mccMnc" }
        mcc = mccMnc.substring(0, 3).toInt()
        mnc = mccMnc.substring(3).toInt()
    }

    override fun equals(o: Any?): Boolean {
        if (o === this) {
            return true
        }
        if (o !is CellInfo) {
            return false
        }
        val ci = o
        return cellRadio == ci.cellRadio && mcc == ci.mcc && mnc == ci.mnc && cid == ci.cid && lac == ci.lac && mSignalStrength == ci.mSignalStrength && mAsu == ci.mAsu && mTa == ci.mTa && psc == ci.psc
    }

    override fun hashCode(): Int { // WTH???
        var result = 17
        result = 31 * result + cellRadio.hashCode()
        result = 31 * result + mcc
        result = 31 * result + mnc
        result = 31 * result + cid
        result = 31 * result + lac
        result = 31 * result + mSignalStrength
        result = 31 * result + mAsu
        result = 31 * result + mTa
        result = 31 * result + psc
        return result
    }

    companion object CREATOR {
        const val CELL_RADIO_UNKNOWN = ""
        const val CELL_RADIO_GSM = "gsm"
        const val CELL_RADIO_WCDMA = "wcdma"
        const val CELL_RADIO_CDMA = "cdma"
        const val CELL_RADIO_LTE = "lte"
        const val UNKNOWN_CID = -1
        const val UNKNOWN_LAC = -1
        const val UNKNOWN_SIGNAL_STRENGTH = -1000
        const val UNKNOWN_ASU = -1
        fun getCellRadioTypeName(networkType: Int): String {
            return when (networkType) {
                TelephonyManager.NETWORK_TYPE_GPRS, TelephonyManager.NETWORK_TYPE_EDGE -> CELL_RADIO_GSM
                TelephonyManager.NETWORK_TYPE_UMTS, TelephonyManager.NETWORK_TYPE_HSDPA, TelephonyManager.NETWORK_TYPE_HSUPA, TelephonyManager.NETWORK_TYPE_HSPA, TelephonyManager.NETWORK_TYPE_HSPAP -> CELL_RADIO_WCDMA
                TelephonyManager.NETWORK_TYPE_LTE -> CELL_RADIO_LTE
                TelephonyManager.NETWORK_TYPE_EVDO_0, TelephonyManager.NETWORK_TYPE_EVDO_A, TelephonyManager.NETWORK_TYPE_EVDO_B, TelephonyManager.NETWORK_TYPE_1xRTT, TelephonyManager.NETWORK_TYPE_EHRPD, TelephonyManager.NETWORK_TYPE_IDEN -> CELL_RADIO_CDMA
                else -> {
                    CELL_RADIO_UNKNOWN
                }
            }
        }

        private fun getRadioTypeName(phoneType: Int): String {
            return when (phoneType) {
                TelephonyManager.PHONE_TYPE_CDMA -> CELL_RADIO_CDMA
                TelephonyManager.PHONE_TYPE_GSM -> CELL_RADIO_GSM
                TelephonyManager.PHONE_TYPE_NONE, TelephonyManager.PHONE_TYPE_SIP ->  // These devices have no radio.
                    ""
                else -> {
                    ""
                }
            }
        }
    }
}