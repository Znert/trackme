package at.ac.tuwien.trackme.services

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.telephony.*
import android.util.Log
import at.ac.tuwien.trackme.HistoryViewModel
import at.ac.tuwien.trackme.TrackMeApplication
import at.ac.tuwien.trackme.data.Measurement
import at.ac.tuwien.trackme.utils.CellInfo
import at.ac.tuwien.trackme.utils.MLSApiResponse
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LocationService : Service() {

    companion object {
        private const val TAG = "LocationService"
    }

    private val binder = LocationServiceBinder()
    lateinit var historyViewModel: HistoryViewModel
    lateinit var context: Context

    inner class LocationServiceBinder : Binder() {
        fun getService(): LocationService = this@LocationService
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    inner class LocationChangeListener(private val callback: (errorMsg: String?) -> Unit) :
        LocationListener {

        override fun onLocationChanged(location: Location?) {
            if (location == null) {
                Log.w(TAG, "No GSP location received")
                callback("Keine GPS-Daten erhalten. Gehen Sie sicher, dass Ihr GPS aktiv ist.")
                return
            }
            Log.i(TAG, "Got GSP location: $location")
            requestGeoLocation(location, callback)
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        }

        override fun onProviderEnabled(provider: String?) {
        }

        override fun onProviderDisabled(provider: String?) {
        }

    }

    @SuppressLint("MissingPermission")
    fun newMeasurement(callback: (errorMsg: String?) -> Unit) {
        (application as TrackMeApplication).getPermissionService()
            .waitForPermission(Manifest.permission.ACCESS_FINE_LOCATION) {
                val locationManager = (application as TrackMeApplication).getLocationManager()
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    callback("Bitte aktivieren Sie GPS.")
                } else {
                    locationManager.requestSingleUpdate(
                        LocationManager.GPS_PROVIDER,
                        LocationChangeListener(callback),
                        Looper.getMainLooper()
                    )
                }
            }
    }

    private fun requestGeoLocation(gpsLocation: Location, callback: (errorMsg: String?) -> Unit) {

        val json = JsonObject()
        json.add("cellTowers", getCellInfo())
        json.add("wifiAccessPoints", getWifiInfo())

        Log.i(TAG, "Sending request to server: \n$json")

        val call: Call<MLSApiResponse> = (application as TrackMeApplication).getMLSAPi()
            .getLocation((application as TrackMeApplication).getApiKeyService().getApiKey(), json)

        call.enqueue(object : Callback<MLSApiResponse> {
            override fun onResponse(
                call: Call<MLSApiResponse>,
                response: Response<MLSApiResponse>
            ) {
                if (!response.isSuccessful) {
                    Log.w(TAG, "Failed to request MLS location: ${response.message()}")
                    callback("Netzwerk-Standort konnte nicht ermittelt werden. Bitte überprüfen Sie den API-Key.")
                } else {

                    val body: MLSApiResponse = response.body()!!

                    val geo = Location("")
                    geo.latitude = body.location.lat
                    geo.longitude = body.location.lng

                    historyViewModel.insert(
                        Measurement(
                            id = 0,
                            timestamp = Calendar.getInstance().time.toString(),
                            gps_longitude = gpsLocation.longitude,
                            gps_latitude = gpsLocation.latitude,
                            precision = gpsLocation.accuracy.toInt(),
                            params = json.toString(),
                            geo_longitude = body.location.lng,
                            geo_latitude = body.location.lat,
                            geo_precision = body.accuracy.toInt(),
                            diff_mls_gps = geo.distanceTo(gpsLocation).toInt()
                        )
                    )

                    callback(null)
                }

            }

            override fun onFailure(call: Call<MLSApiResponse>, throwable: Throwable) {
                println(throwable)
            }
        })
    }

    private fun getCellInfo(): JsonArray {
        val tm: TelephonyManager =
            context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val list: List<android.telephony.CellInfo> = tm.allCellInfo

        val cells: MutableList<CellInfo> = mutableListOf()
        for (cell in list) {
            addCellToList(cells, cell, tm)
        }

        val cellJsonArray = JsonArray()

        cells.forEach {
            cellJsonArray.add(it.toJsonObject())
        }

        return cellJsonArray
    }

    private fun getWifiInfo(): JsonArray {
        val wifiManager: WifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val results: List<ScanResult> = wifiManager.scanResults
        val wifiInfo: MutableList<JsonObject> = mutableListOf()
        for (result in results) {
            val resultInfo = JsonObject()
            resultInfo.addProperty("macAddress", result.BSSID)
            resultInfo.addProperty("name", result.SSID)
            resultInfo.addProperty("frequency", result.frequency)
            resultInfo.addProperty("signalStrength", result.level)
            wifiInfo.add(resultInfo)
        }


        val json = JsonArray()

        wifiInfo.forEach {
            json.add(it)
        }

        return json
    }

    /**
     * This code was taken and modified from the MozStumbler app:
     *
     * https://github.com/mozilla/MozStumbler/blob/dev/libraries/stumbler/src/main/java/org/mozilla/mozstumbler/service/stumblerthread/scanners/cellscanner/SimpleCellScannerImplementation.java
     *
     */
    private fun addCellToList(
        cells: MutableList<CellInfo>,
        observedCell: android.telephony.CellInfo?,
        tm: TelephonyManager
    ): Boolean {
        if (tm.phoneType == 0) {
            return false
        }
        var added = false
        if (observedCell is CellInfoGsm) {
            val ident = observedCell.cellIdentity
            if (ident.mcc != Int.MAX_VALUE && ident.mnc != Int.MAX_VALUE) {
                val strength =
                    observedCell.cellSignalStrength
                val cell = CellInfo()
                cell.setGsmCellInfo(
                    ident.mcc,
                    ident.mnc,
                    ident.lac,
                    ident.cid,
                    strength.asuLevel
                )
                cells.add(cell)
                added = true
            }
        } else if (observedCell is CellInfoCdma) {
            val cell = CellInfo()
            val ident = observedCell.cellIdentity
            val strength =
                observedCell.cellSignalStrength
            cell.setCdmaCellInfo(
                ident.basestationId,
                ident.networkId,
                ident.systemId,
                strength.dbm
            )
            cells.add(cell)
            added = true
        } else if (observedCell is CellInfoLte) {
            val ident = observedCell.cellIdentity
            if (ident.mnc != Int.MAX_VALUE && ident.mcc != Int.MAX_VALUE) {
                val cell = CellInfo()
                val strength =
                    observedCell.cellSignalStrength
                cell.setLteCellInfo(
                    ident.mcc,
                    ident.mnc,
                    ident.ci,
                    ident.pci,
                    ident.tac,
                    strength.asuLevel,
                    strength.timingAdvance
                )
                cells.add(cell)
                added = true
            }
        } else if (observedCell is CellInfoWcdma) {
            val ident = observedCell.cellIdentity
            if (ident.mnc != Integer.MAX_VALUE && ident.mcc != Integer.MAX_VALUE) {
                val cell = CellInfo()
                val strength = observedCell.cellSignalStrength
                cell.setWcdmaCellInfo(
                    ident.mcc,
                    ident.mnc,
                    ident.lac,
                    ident.cid,
                    ident.psc,
                    strength.asuLevel
                )
                cells.add(cell)
                added = true
            }
        }
        return added
    }

}