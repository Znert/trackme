package at.ac.tuwien.trackme.utils

import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation


/**
 * Taken and modified from https://medium.com/better-programming/recyclerview-expanded-1c1be424282c
 */
class AnimationUtil {
    companion object {
        fun expand(view: View) {

            // Measurement taken from https://stackoverflow.com/a/13381228/6643866
            val matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(
                (view.parent as View).width,
                View.MeasureSpec.EXACTLY
            )
            val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(
                0,
                View.MeasureSpec.UNSPECIFIED
            )
            view.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
//            val targetHeight: Int = view.measuredHeight
//            view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            val actualHeight = view.measuredHeight
            view.layoutParams.height = 0
            view.visibility = View.VISIBLE
            val animation: Animation =
                object : Animation() {
                    override fun applyTransformation(
                        interpolatedTime: Float,
                        t: Transformation?
                    ) {
                        view.layoutParams.height =
                            if (interpolatedTime == 1f) {
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            } else {
                                (actualHeight * interpolatedTime).toInt()
                            }
                        view.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }

                }
            animation.duration =
                (actualHeight / view.context.resources.displayMetrics.density).toLong()
            view.startAnimation(animation)
        }

        fun collapse(view: View) {
            val actualHeight: Int = view.measuredHeight
            val animation = object : Animation() {
                override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation?
                ) {
                    if (interpolatedTime == 1f) {
                        view.visibility = View.GONE
                    } else {
                        view.layoutParams.height =
                            actualHeight - (actualHeight * interpolatedTime).toInt()
                        view.requestLayout()
                    }
                }
            }
            animation.duration =
                (actualHeight / view.context.resources.displayMetrics.density).toLong()
            view.startAnimation(animation)
        }
    }
}