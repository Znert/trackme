package at.ac.tuwien.trackme.services

import android.util.Base64
import at.ac.tuwien.trackme.TrackMeApplication
import at.ac.tuwien.trackme.utils.EncryptionUtil

class ApiKeyService(
    private val application: TrackMeApplication,
    private val mockedExternalWebServer: MockedExternalWebServer
) {

    fun getApiKey(): String {

        val apiKeyPreference = application.getSharedPreferences().getString(TrackMeApplication.SETTINGS.API_KEY, "")
        if (apiKeyPreference.isNullOrEmpty()) {
            val key = mockedExternalWebServer.requestApiKey()

            if (EncryptionUtil.isEncrypted(application.applicationContext)) {
                val encrypted = EncryptionUtil.encrypt(key.toByteArray(), TrackMeApplication.SETTINGS.API_KEY)
                application.getSharedPreferences().edit()
                    .putString(TrackMeApplication.SETTINGS.API_KEY, String(Base64.encode(encrypted, Base64.DEFAULT))).apply()

            } else {

                application.getSharedPreferences().edit()
                    .putString(TrackMeApplication.SETTINGS.API_KEY, key).apply()

            }
            return key
        }

        if (EncryptionUtil.isEncrypted(application.applicationContext)) {
            return String(EncryptionUtil.decrypt(Base64.decode(apiKeyPreference, Base64.DEFAULT), TrackMeApplication.SETTINGS.API_KEY))
        }

        return apiKeyPreference
    }

    fun encryptApiKey() {

        val apiKeyPreference = application.getSharedPreferences().getString(TrackMeApplication.SETTINGS.API_KEY, "")
        if (apiKeyPreference.isNullOrEmpty()) {
            return
        }

        val encrypted = EncryptionUtil.encrypt(apiKeyPreference.toByteArray(), TrackMeApplication.SETTINGS.API_KEY)
        application.getSharedPreferences().edit()
            .putString(TrackMeApplication.SETTINGS.API_KEY, String(Base64.encode(encrypted, Base64.DEFAULT))).apply()

    }

}