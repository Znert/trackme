package at.ac.tuwien.trackme.services

/**
 * This class is a mock of an external web server serving the api key on request.
 * It is assumed that a secure transport protocol (e.g. https) is used.
 */
class MockedExternalWebServer {

    /**
     * Request the API key from the server.
     *
     * Note that an actual implementation would be asynchronous.
     * However, for simplicity it returns the key immediately.
     */
    fun requestApiKey(): String {
        return "test"
    }

}