package at.ac.tuwien.trackme


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import at.ac.tuwien.trackme.services.PermissionService
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

/**
 * A simple [Fragment] subclass.
 */
class MeasurementFragment : Fragment() {

    private lateinit var locationManager: LocationManager
    private lateinit var map: SupportMapFragment
    private lateinit var permissionService: PermissionService

    override fun onAttach(context: Context) {
        super.onAttach(context)

        // Check if the activity, that created this fragment is of type PermissionServiceManager
        val permissionServiceProvider = this.activity as? PermissionService.PermissionServiceManager
            ?: throw ClassCastException("The activity instantiating ${this.javaClass} must implement ${PermissionService.PermissionServiceManager::class}")

        // Use the PermissionServiceManager to get a reference to the PermissionService
        permissionService = permissionServiceProvider.getPermissionService()

        // Get a reference to the location manager
        locationManager = (activity!!.application as TrackMeApplication).getLocationManager()

    }

    @SuppressLint("MissingPermission")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // First, create the view for the measurement fragment.
        val view = inflater.inflate(R.layout.measurement, container, false)
        // Get a reference to the map fragment
        map = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        // Finally, init the map to show the current user location
        initMap()

        return view
    }

    /**
     * This function will init the map to show the current users position as well as showing the
     * location of the user.
     */
    private fun initMap() {
        // Wait for the location permission
        permissionService.waitForPermission(Manifest.permission.ACCESS_FINE_LOCATION) {
            // Now request a single location update
            val location = this.getLastKnownLocation()

            map.getMapAsync(MapInitializer(location))
        }
    }

    /**
     * Modified from https://stackoverflow.com/a/9873351/6643866
     */
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(): Location? {
        val providers: List<String> = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l = locationManager.getLastKnownLocation(provider) ?: continue

            if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                bestLocation = l
            }
        }
        return bestLocation
    }

    /**
     * This class wil initialize the map fragment to view the current users location.
     */
    private class MapInitializer(val location: Location?) : OnMapReadyCallback {

        override fun onMapReady(mapInstance: GoogleMap) {
            // Enable location tracking on the map
            mapInstance.isMyLocationEnabled = true

            if (location != null) {
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    LatLng(location.latitude, location.longitude),
                    15f
                )
                // Move the camera to the current position
                mapInstance.moveCamera(cameraUpdate)
            }
        }
    }


}
