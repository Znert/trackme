package at.ac.tuwien.trackme

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import at.ac.tuwien.trackme.data.Measurement
import at.ac.tuwien.trackme.data.source.TrackMeDatabase
import kotlinx.coroutines.launch

class HistoryViewModel(application: Application) : AndroidViewModel(application) {

    fun insert(measurement: Measurement) = viewModelScope.launch {
        val dao = TrackMeDatabase.getDatabase(getApplication()).measurementDao()
        dao.insertMeasurement(measurement)
    }

    fun delete(measurement: Measurement) = viewModelScope.launch {
        TrackMeDatabase.getDatabase(getApplication()).measurementDao().deleteMeasurement(measurement)
    }

    fun getAllMeasurements(): LiveData<List<Measurement>> {
        return TrackMeDatabase.getDatabase(getApplication()).measurementDao().getMeasurements()
    }

}