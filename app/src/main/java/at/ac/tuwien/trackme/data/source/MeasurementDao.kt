package at.ac.tuwien.trackme.data.source

import androidx.lifecycle.LiveData
import androidx.room.*
import at.ac.tuwien.trackme.data.Measurement

@Dao
interface MeasurementDao {

    @Query("SELECT * FROM Measurements")
    fun getMeasurements(): LiveData<List<Measurement>>

    @Query("SELECT * FROM Measurements WHERE id = :measurementId")
    suspend fun getMeasurementById(measurementId: Int): Measurement?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMeasurement(measurement: Measurement)

    @Delete
    suspend fun deleteMeasurement(measurement: Measurement)

    @Query("DELETE FROM measurements")
    suspend fun deleteAll()

}