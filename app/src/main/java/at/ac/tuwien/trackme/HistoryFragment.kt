package at.ac.tuwien.trackme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 * A simple [Fragment] subclass.
 */
class HistoryFragment : Fragment() {

    private lateinit var historyViewModel: HistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.history, container, false) as FrameLayout
        val list = view.findViewById(R.id.history_list) as RecyclerView


        historyViewModel = ViewModelProviders.of(activity!!).get(HistoryViewModel::class.java)

        val adapter = RecycleAdapter(activity!!, historyViewModel)
        adapter.recyclerView = list
        //adapter.setHasStableIds(true)
        list.adapter = adapter
        val layoutManager = LinearLayoutManager(activity!!)
        layoutManager.recycleChildrenOnDetach = true
        list.layoutManager = LinearLayoutManager(activity!!)

        historyViewModel.getAllMeasurements().observe(this, Observer { measurements ->
            measurements?.let { adapter.setMeasurements(it) }
        })

        return view
    }



}
