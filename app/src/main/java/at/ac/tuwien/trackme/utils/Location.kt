package at.ac.tuwien.trackme.utils

class Location(var lat: Double, var lng: Double)