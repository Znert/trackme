package at.ac.tuwien.trackme.data.source

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import at.ac.tuwien.trackme.TrackMeApplication
import at.ac.tuwien.trackme.data.Measurement
import at.ac.tuwien.trackme.utils.EncryptionUtil
import com.commonsware.cwac.saferoom.SafeHelperFactory


@Database(entities = [Measurement::class], version = 1, exportSchema = false)
abstract class TrackMeDatabase : RoomDatabase() {

    abstract fun measurementDao(): MeasurementDao

    companion object {

        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        @Volatile
        var INSTANCE: TrackMeDatabase? = null

        @Synchronized
        fun getDatabase(context: Context): TrackMeDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }

            val pw = EncryptionUtil.getDBPassword(context)
            val factory = SafeHelperFactory(pw)

            val instance = Room.databaseBuilder(
                context.applicationContext,
                TrackMeDatabase::class.java,
                TrackMeApplication.CONSTANTS.DATABASE_NAME
            )
                .openHelperFactory(factory)
                .build()
            INSTANCE = instance

            return instance
        }

        @Synchronized
        fun encryptDatabase(context: Context) {
            INSTANCE?.close()
            INSTANCE = null

            EncryptionUtil.createEncryptedDB(context)
        }

    }

}