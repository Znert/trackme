package at.ac.tuwien.trackme.services

import at.ac.tuwien.trackme.utils.MLSApiResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface MLSApi {

    @POST("v1/geolocate")
    fun getLocation(@Query("key") key: String, @Body json: JsonObject): Call<MLSApiResponse>

}