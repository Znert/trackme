package at.ac.tuwien.trackme

import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import androidx.core.content.ContextCompat
import at.ac.tuwien.trackme.services.ApiKeyService
import at.ac.tuwien.trackme.services.MLSApi
import at.ac.tuwien.trackme.services.MockedExternalWebServer
import at.ac.tuwien.trackme.services.PermissionService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class TrackMeApplication : android.app.Application() {

    private val baseURL: String = "https://location.services.mozilla.com/"

    object SETTINGS {
        const val FILE_NAME = "trackme.settings"
        const val SETTING_ENCRYPTED_DB = "encrypted-db"
        const val API_KEY = "api-key"
    }

    object CONSTANTS {
        const val DATABASE_NAME = "trackMe_database"
    }

    private lateinit var mlsApi: MLSApi
    private lateinit var locationManager: LocationManager
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var permissionService: PermissionService
    private lateinit var mockedExternalWebServer: MockedExternalWebServer
    private lateinit var apiKeyService: ApiKeyService

    open fun getMLSAPi(): MLSApi {
        if (this::mlsApi.isInitialized) {
            return mlsApi
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(this.baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        mlsApi = retrofit.create(MLSApi::class.java)

        return mlsApi;
    }

    open fun getLocationManager(): LocationManager {
        if (this::locationManager.isInitialized) {
            return locationManager
        }
        locationManager = ContextCompat.getSystemService(
            this,
            LocationManager::class.java
        ) as LocationManager
        return locationManager
    }

    open fun getSharedPreferences(): SharedPreferences {
        if (this::sharedPreferences.isInitialized) {
            return sharedPreferences
        }

        sharedPreferences = super.getSharedPreferences(SETTINGS.FILE_NAME, Context.MODE_PRIVATE)
        return sharedPreferences
    }

    open fun getPermissionService(): PermissionService {
        if (this::permissionService.isInitialized) {
            return permissionService
        }

        throw IllegalAccessException("No PermissionService initialized.")
    }

    open fun setPermissionService(
        context: Context,
        requester: PermissionService.PermissionRequester
    ) {
        permissionService = PermissionService(context, requester)

    }

    /**
     * Returns a Mock of an external web server to demonstrate the fetching of the api key.
     */
    open fun getBackendService(): MockedExternalWebServer {
        if (this::mockedExternalWebServer.isInitialized) {
            return mockedExternalWebServer
        }
        return MockedExternalWebServer()
    }

    open fun getApiKeyService(): ApiKeyService {
        if (this::apiKeyService.isInitialized) {
            return apiKeyService
        }
        return ApiKeyService(this, getBackendService())
    }

}