package at.ac.tuwien.trackme

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import at.ac.tuwien.trackme.data.Measurement
import android.widget.Toast
import at.ac.tuwien.trackme.utils.SwipeRevealLayout
import android.content.Intent
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.content.FileProvider
import at.ac.tuwien.trackme.utils.AnimationUtil
import java.io.File


class RecycleAdapter(private val context: Context, private val historyViewModel: HistoryViewModel) : RecyclerView.Adapter<RecycleAdapter.MeasurementViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    var measurements = emptyList<Measurement>()
    lateinit var recyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeasurementViewHolder {
        val itemView = inflater.inflate(R.layout.list_item_measurement, parent, false)

        return MeasurementViewHolder(context, itemView, historyViewModel)
    }

    override fun onBindViewHolder(holder: MeasurementViewHolder, position: Int) {
        val current = measurements[position]
        holder.setData(current)
    }

    override fun getItemCount() = measurements.size


    internal fun setMeasurements(measurements: List<Measurement>) {
        this.measurements = measurements
        notifyDataSetChanged()
    }

    inner class MeasurementViewHolder (private var context: Context, itemView: View, historyViewModel: HistoryViewModel): RecyclerView.ViewHolder (itemView) {
        private var currentMeasurement: Measurement? = null

        private val swipeRevealLayout: SwipeRevealLayout = itemView.findViewById(R.id.swipe_reveal)
        private val card: CardView = itemView.findViewById(R.id.card)
        private val cardTitle: LinearLayout = itemView.findViewById(R.id.card_title)
        private val details: LinearLayout = itemView.findViewById(R.id.details)
        private var isExpanded = details.visibility == View.VISIBLE

        init {

            itemView.findViewById<ImageButton>(R.id.delete_button).setOnClickListener { v ->

                historyViewModel.delete(this.currentMeasurement!!)
                itemView.findViewById<SwipeRevealLayout>(R.id.swipe_reveal).close(true)
                //historyViewModel.refresh()
                Toast.makeText(
                    v.context,
                    "Bericht #" + this.currentMeasurement!!.id + " gelöscht.",
                    Toast.LENGTH_SHORT
                ).show()
            }

            itemView.findViewById<ImageButton>(R.id.share_button).setOnClickListener { v ->

                val filename = "bericht_nr${currentMeasurement!!.id}.txt"
                var newfile = File(context.filesDir, filename)
                newfile.writeText(this.currentMeasurement!!.toString())
                var uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", newfile)

                val i = Intent(Intent.ACTION_SEND)
                i.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                i.type = "plain/text"
                i.putExtra(Intent.EXTRA_STREAM, uri)
                context.startActivity(Intent.createChooser(i, "Send email..."));

                context.startActivity(i)

            }

            card.setOnClickListener {

                isExpanded = !isExpanded

                // FYI, this works too with animateLayoutChanges=true, but the height of the item
                // is always changed instantly, causing the next item overlapping the current one on
                // collapse
//                TransitionManager.beginDelayedTransition(card)
//
//                details.visibility = if (isExpanded) {
//                    View.VISIBLE
//                } else {
//                    View.GONE
//                }

                if (isExpanded) {
                    AnimationUtil.expand(details)
                } else {
                    AnimationUtil.collapse(details)
                }

            }
        }

        fun setData(measurement: Measurement?) {

            measurement?.let {
                val idTextView = itemView.findViewById(R.id.measurement_id) as TextView
                val dateTextView = itemView.findViewById(R.id.measurement_date) as TextView
                val gpsLongitudeTextView = itemView.findViewById(R.id.measurement_longitude) as TextView
                val gpsLatitudeTextView = itemView.findViewById(R.id.measurement_latitude) as TextView
                val geoLongitudeTextView = itemView.findViewById(R.id.geo_longitude) as TextView
                val geoLatitudeTextView = itemView.findViewById(R.id.geo_latitude) as TextView
                val diffTextView = itemView.findViewById<TextView>(R.id.geo_diff)
                val geoParamTextView = itemView.findViewById<TextView>(R.id.geo_params)
                val precisionTextView = itemView.findViewById<TextView>(R.id.precision)
                val geoPrecisionTextView = itemView.findViewById<TextView>(R.id.geo_precision)


                idTextView.text = context.resources.getString(R.string.measurement_id, measurement.id)
                dateTextView.text = context.resources.getString(R.string.date, measurement.timestamp)
                gpsLongitudeTextView.text = context.resources.getString(R.string.gps_longitude, measurement.gps_longitude)
                gpsLatitudeTextView.text = context.resources.getString(R.string.gps_latitude, measurement.gps_latitude)
                geoLatitudeTextView.text = context.resources.getString(R.string.geo_latitude, measurement.geo_latitude)
                geoLongitudeTextView.text = context.resources.getString(R.string.geo_longitude, measurement.geo_longitude)
                diffTextView.text = context.resources.getString(R.string.geo_diff, measurement.diff_mls_gps)
                geoParamTextView.text = context.resources.getString(R.string.geo_params, measurement.params)
                precisionTextView.text = context.resources.getString(R.string.precision, measurement.precision)
                geoPrecisionTextView.text = context.resources.getString(R.string.precision, measurement.geo_precision)


                this.currentMeasurement = measurement
            }

        }
    }
}